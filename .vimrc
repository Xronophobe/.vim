
"execute pathogen#infect()
set nocompatible
filetype off
"filetype plugin on
"filetype indent on


" set the runtime path to include Vundle and initialize
set rtp+=~/.vim/bundle/Vundle.vim

call vundle#begin()
" alternatively, pass a path where Vundle should install plugins
"call vundle#begin('~/some/path/here')

" let Vundle manage Vundle, required
Plugin 'gmarik/Vundle.vim'

Plugin 'bling/vim-airline'
Plugin 'kien/ctrlp.vim'
Plugin 'tacahiroy/ctrlp-funky'
"Plugin 'flazz/vim-colorschemes'
Plugin 'Lokaltog/vim-easymotion'
Plugin 'tpope/vim-fugitive.git'
"Plugin 'eagletmt/ghcmod-vim'
"Plugin 'eagletmt/neco-ghc'
"Plugin 'davidhalter/jedi-vim'
Plugin 'nanotech/jellybeans.vim'
Plugin 'Shougo/neocomplcache.vim'
Plugin 'scrooloose/nerdcommenter'
Plugin 'scrooloose/nerdtree'
Plugin 'jistr/vim-nerdtree-tabs'
"Plugin 'powerline/powerline'
Plugin 'tpope/vim-surround'
Plugin 'scrooloose/syntastic'
Plugin 'majutsushi/tagbar'
"Plugin 'Valloric/YouCompleteMe'

" All of your Plugins must be added before the following line
call vundle#end()
filetype plugin indent on

"
"Brief help
" :PluginList       - lists configured plugins
" :PluginInstall    - installs plugins; append `!` to update or just
" :PluginUpdate
" :PluginSearch foo - searches for foo; append `!` to refresh local cache
" :PluginClean      - confirms removal of unused plugins; append `!` to
"  auto-approve removal
"
" see :h vundle for more details or wiki for FAQ
" Put your non-Plugin stuff after this line

syntax on

set bs=2
set number
set ruler

set autoindent
set tabstop=4
set softtabstop=4
set shiftwidth=4
set shiftround
set expandtab

set tw=82
set colorcolumn=-2
highlight ColorColumn ctermbg=green
""vnoremap < <gv "better indentation
""vnoremap > >gv "better indentation

"CtrlP options
let g:ctrlp_show_hidden = 1

"CtrlP Funky
nnoremap <Leader>fu :CtrlPFunky<Cr>

"Jedi-VIM options:
let g:jedi#use_splits_not_buffers = "right"
let g:jedi#show_call_signatures = "1"

"Necoghc
au BufRead,BufNewFile *.hs setlocal omnifunc=necoghc#omnifunc

"NerdTree
autocmd vimenter * NERDTree
map <F2> :NERDTreeToggle<CR>
let g:nerdtree_tabs_open_on_console_startup=1
let g:NERDTreeDirArrows=0
autocmd VimEnter * wincmd l
autocmd BufNew   * wincmd l

"VIM powerline
set rtp+=/Users/rzyel/.vim/bundle/powerline/powerline/bindings/vim
set laststatus=2

"Syntastic
set statusline+=%#warningmsg#
set statusline+=%{SyntasticStatuslineFlag()}
set statusline+=%*

let g:syntastic_always_populate_loc_list = 1
let g:syntastic_auto_loc_list = 1
let g:syntastic_check_on_open = 1
let g:syntastic_check_on_wq = 0
